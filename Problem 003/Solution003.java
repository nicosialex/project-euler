public class Solution003 {

    public static void main(String args[]) {
        System.out.println(largestPrimeFactor(600851475143L));
    }

    /**
     * For the problem my approach was to looking continuosly for a divider. When I
     * found it I simply decided to divide the initial value as many time as
     * possible with the founded divider (so to reduce the iteration done), until
     * I'll reach the maximum possible.
     * 
     */
    private static int largestPrimeFactor(long n) {
        /*
         * I want to proceed only for the odd value. So, on first, I'll make the job for
         * the even numbers
         */
        long searchedValue = n;
        int maxDiv = 1;
        if (searchedValue % 2 == 0) {
            while (searchedValue % 2 == 0) {
                searchedValue = searchedValue / 2;
            }
            maxDiv = 2;
        }
        int i;
        for (i = 3; i <= searchedValue; i = i + 2) {
            if (searchedValue % i == 0) {
                maxDiv = i;
                searchedValue = searchedValue / i;
                while (searchedValue % i == 0) {
                    searchedValue = searchedValue / i;
                }
            }
        }
        return maxDiv;
    }

}