public class Solution004 {
    public static void main(String args[]) {
        System.out.println(findMaxPalindrome());
    }

    /**
     * Starting from the biggest 3 digit number (999) and decreasing one of the two
     * members, I'll check if the number is palindrome then compare the number with
     * maximum. I'll interrupt the flow of the inner loop when I'll found a
     * palindrome (because it would be the maximum for the inner loop). Moreover
     * I'll interrupt the flow of the outer loop when the square of the current
     * number is lower than the current max.
     */
    private static int findMaxPalindrome() {
        int max = 0;
        for (int i = 999; i >= 100; i--) {
            for (int j = i; j >= 100; j--) {
                int temp = i * j;
                if (Utilities.isPalindrome(temp)) {
                    if (temp > max)
                        max = temp;
                    break;
                }
            }
            if (i * i < max)
                break;
        }
        return max;
    }

}