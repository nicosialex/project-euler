public class Solution007 {
    public static void main(String args[]) {
        System.out.println(findTheNthPrime(10001));
    }

    /**
     * Simply check all the prime number until I'll find the n-th
     */
    private static int findTheNthPrime(int n) {
        int currentPrime = 3;
        int primePosition = 2;
        while (primePosition < n) {
            currentPrime += 2;
            if (Utilities.isPrime(currentPrime)) {
                primePosition++;
            }
        }
        return currentPrime;
    }
}