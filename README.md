# Project Euler

This repository is indeed to contain my solution implementation
for the problems published at https://projecteuler.net/.

For every problem I'll try to create a solution in Java (the language I'm most skilled at)
and, If I'll have the time, I'll also try to create more solutions in order to make a performance comparison.

This repository is made with the scope of demonstrate my problem solving skills and not to provide a way to evaluate my style of writing code (I won't use any pattern/class structure inside the project).

I'll publish only the first 100 problems due to the restrictions expressed by the owner of the website.

Enjoy!