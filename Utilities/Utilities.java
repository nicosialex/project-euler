public class Utilities {
    /**
     * A really basic method to verify if a number is prime or not
     * 
     * @param n the number to be verified
     * @return <code>true</code> if the number is prime, <code>false</code>
     *         otherwise
     */
    public static boolean isPrime(final int n) {
        if (n % 2 == 0 && n != 2)
            return false;
        for (int i = 3; i <= n / 2; i = i + 2)
            if (n % i == 0)
                return false;
        return true;
    }

    /**
     * Verify if the passed number is a palindrome or not
     * 
     * @param n the input parameter
     * @return <code>true</code> if the number is a palindrome, <code>false</code>
     *         otherwise.
     */
    public static boolean isPalindrome(final int n) {
        final String number = "" + n;
        for (int i = 0; i < number.length() + 1 / 2; i++)
            if (number.charAt(i) != number.charAt(number.length() - i - 1))
                return false;
        return true;
    }
}