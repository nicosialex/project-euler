public class Solution005 {

    public static void main(String args[]) {
        System.out.println(getSmallestMultiple(20));
    }

    /**
     * This problem is really easy written by hand but could be really hard
     * programmatically. We have to find all the prime factors for all the numbers
     * from 1 to n. Then multiply them together. We should also consider that some
     * prime factors are common but with a different weight (for example the 2 would
     * be a prime factor for the number 4 and the number 8, but in the 4 it's
     * present 2 times and in the 8 it's present 3 times. We should consider the 2
     * only 3 times).
     * 
     */
    private static long getSmallestMultiple(int n) {
        long smallestMultiple = 1;
        int primeFactors[] = new int[n];
        for (int i = 1; i <= n; i++) {
            for (int j = 2; j <= n; j++)
                if (Utilities.isPrime(j) && i % j == 0) {
                    int temp = i;
                    int count = 0;
                    while (temp % j == 0) {
                        temp = temp / j;
                        count++;
                    }
                    primeFactors[j - 1] = Math.max(primeFactors[j - 1], count);
                }
        }
        for (int i = 1; i <= n; i++)
            smallestMultiple = smallestMultiple * (long) Math.pow(i, primeFactors[i - 1]);
        return smallestMultiple;
    }

}