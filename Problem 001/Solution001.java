public class Solution001 {
    public static void main(final String args[]) {
        System.out.println(sum3And5MultiplesUnderValue(999));
    }

    /**
     * I wanted to develop the code without loop. For this reason I retrieved a
     * mathematical formula which can evaluate the sum of all natural numbers from 1
     * to n which is: 1+ 2+ ... + n = n(n+1) / 2. So on first we need to know how
     * many times a single number (3 or 5) should be considered between the number 1
     * and a generic N and this kind of information could be obtained dividing N
     * rispectively by 3 and 5. After that, we could sum the series result
     * multiplied by 3 and 5 but the result would be still wrong. We should remove
     * the commons multiplier to the two series, so we have to make the same
     * reasoning for the number 15 and removing the sum from the previous result.
     */
    private static int sum3And5MultiplesUnderValue(final int value) {
        final int numberOf5Iteration = value / 5;
        final int numberOf3Iteration = value / 3;
        final int numberOf15Iteration = value / 15;
        int sum = (numberOf3Iteration * (numberOf3Iteration + 1)) / 2 * 3
                + (numberOf5Iteration * (numberOf5Iteration + 1)) / 2 * 5
                - (numberOf15Iteration * (numberOf15Iteration + 1)) / 2 * 15;
        return sum;
    }

}