public class Solution002 {

    public static void main(String args[]) {
        System.out.println(fibonacciEvenSeriesUnderN(4000000));
    }

    /**
     * The Fibonacci series is created by summing a number with is previous inside
     * the series in order to create the next element of the series. I wanted to
     * generalize the method, so passed a number n we would evaluate every element
     * of the series which is lower than the passed n. Then, inside the loop, we
     * should sum only the even element of the series.
     */
    private static int fibonacciEvenSeriesUnderN(int n) {
        int previous = 1;
        int current = 2;
        int sum = 0;
        while (current < n) {
            if (current % 2 == 0)
                sum += current;
            int temp = previous;
            previous = current;
            current += temp;
        }
        return sum;
    }

}