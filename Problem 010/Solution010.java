import java.util.ArrayList;
import java.util.List;

public class Solution010 {
    public static void main(String args[]) {
        System.out.println(sumOfPrimesBelowN(2000000));
    }

    /**
     * My approach was to find all the prime number then summing them. I created an
     * arraylist which will be populated with prime number while I discover them.
     * The next number will be identified as prime by trying to divide them with the
     * previously found prime number.
     * 
     */
    private static long sumOfPrimesBelowN(int n) {
        List<Integer> primeNumbers = new ArrayList<>();
        primeNumbers.add(2);
        long sumOfPrimes = 2;
        for (int i = 3; i < n; i = i + 2) {
            boolean isPrime = true;
            for (Integer prime : primeNumbers)
                if (i % prime == 0) {
                    isPrime = false;
                    break;
                }
            if (isPrime) {
                System.out.println(i);
                primeNumbers.add(i);
                sumOfPrimes += i;
            }
        }
        return sumOfPrimes;
    }
}