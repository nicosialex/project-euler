public class Solution006 {

    public static void main(String args[]) {
        System.out.println(differenceBetweenSquareOfSumAndSumOfSquare(100));
    }

    /**
     * In this case the requested number is so low that I decided to simply adding
     * every square for single number. For the square of the sum I used the formula
     * to find the sum for the first N number, which is: 1 + 2 + .... N = (N+1) * N
     * / 2. With some math we could find a similar formula maybe considering the
     * series development
     */
    private static long differenceBetweenSquareOfSumAndSumOfSquare(int n) {
        long sumSquared = n * (n + 1) / 2;
        long squareSummed = 0;
        for (int i = 1; i <= n; i++) {
            squareSummed += (i * i);
        }
        sumSquared *= sumSquared;
        return sumSquared - squareSummed;
    }
}