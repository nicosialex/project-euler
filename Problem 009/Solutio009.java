public class Solutio009 {
    public static void main(String args[]) {
        System.out.println(solvePythagoreanTriplet());
    }

    /**
     * Thre are several way to solve the problem. I'll try with brute force. Making
     * some substitution on the starting formula we have that a + b - (a*b) / 1000 =
     * 500. So a*b must return 0 as % 1000 operation and the two term should respect
     * the above formula. After founded a and b we could find c making the
     * difference between 1000 and the two terms founded previously.
     */
    private static int solvePythagoreanTriplet() {
        for (int b = 1; b < 1000; b++) {
            for (int a = 1; a < b; a++) {
                if ((a * b) % 1000 == 0 && (a + b - (a * b) / 1000) == 500) {
                    System.out.println("a:" + a + " b:" + b);
                    int c = 1000 - a - b;
                    return a * b * c;
                }
            }
        }
        return 0;
    }
}